import './src/components/principal/ButtonComponent';
import './src/components/principal/HeaderComponent';
import './src/components/principal/FooterComponent';
import './src/components/principal/MainPrincipalComponent';
import './src/components/principal/TarjetasComponent';

import './src/components/intro/CreateAccountComponent';
import './src/components/intro/FooterIntro';
import './src/components/intro/HeaderInicio';
import './src/components/intro/HeaderIntro'
import './src/components/intro/InputComponent';
import './src/components/intro/IntroComponent';
import './src/components/intro/LoginComponent';
import './src/components/intro/NoInternetConnectionComponent';

import './src/components/solicitar/ElegirComponent';
import './src/components/solicitar/ExitoComponent';
import './src/components/solicitar/ResumenSolicitud';
import './src/components/solicitar/SeleccionarComponent';
import './src/components/solicitar/SolicitarComponent';
