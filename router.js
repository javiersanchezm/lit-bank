import { Router } from '@vaadin/router';

import './src/components/principal/TarjetasComponent.js'
import './src/components/solicitar/ExitoComponent.js';
import './index.js';


const outlet = document.querySelector('output');
const router = new Router(outlet);

router.setRoutes([
    { path: '/', component: 'intro-component'},
    { path: '/login', component: 'login-component'},
    { path: '/crear-cuenta', component: 'create-account-component' },
    { path: '/tarjetas-principal', component: 'main-principal-component' },
    { path: '/tarjetas', component: 'tarjetas-component' },
    { path: '/solicitar', component: 'solicitar-component' },
    { path: '/seleccionar', component: 'seleccionar-component' },
    { path: '/elegir', component: 'elegir-component' },
    { path: '/resumen-solicitud', component: 'resumen-solicitud' },
    { path: '/exito', component: 'exito-component' },

    { path: '**', component: 'no-internet-conection-component'},
])

