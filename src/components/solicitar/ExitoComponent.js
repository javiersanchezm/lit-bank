import { LitElement, html, css } from 'lit';
import union from '/images/Union.svg';

export class ExitoComponent extends LitElement {
    static styles = [
        css`
            :host {
                display: block;
                width: 100%;
                //height: calc( 100vh - 160px );
            }

            main{
                width: 600px;
                height: 450px;
                margin: auto;
                display: flex;
                flex-direction: column;
                justify-content: space-evenly;
                min-height: calc( 100vh - 160px );
                background-color: var(--color-cloud);
            }

            div{
                width: 100%;
                text-align: center;
            }

            h1{
                font-family: 'Inter';
                font-weight: 700;
                font-size: 36px;
                color: var(--color-sea);
            }

            p{
                width: 550px;
                height: 96px;
                font-family: 'Inter';
                font-weight: 400;
                font-size: 24px;
                color: var(--color-carbon);
            }

            @media (min-width: 300px) and (max-width: 960px){
                img{
                    //display: none;
                }

                main{
                    width: 300px;
                    height: calc(100vh - 160px);
                    margin: auto;
                }

                h1{
                    font-weight: 700;
                    font-size: 24px;
                    color: var(--color-sea);
                }

                p{
                    width: 278px;
                    height: 64px;
                    font-size: 16px;
                }
            }
        `
    ];

    render() {
        return html`
            <header-component></header-component>

            <main>
                <div>
                    <img src="${union}">
                    <h1>¡Listo!</h1>
                    <p>Ya puedes usar tu tarjeta. También puedes ver y administrar tu tarjeta en la sección de "Mis tarjetas"</p>
                </div>

                <button-component url="/tarjetas" info="Continuar"></button-component>
            </main>

            <footer-component></footer-component>
        `;
    }
}

customElements.define('exito-component', ExitoComponent);