import { LitElement, html, css } from "lit";
import debito from '/images/debito.svg';
import credito from '/images/credito.svg';
import Swal from 'sweetalert2';

export class SeleccionarComponent extends LitElement{

    static styles = css`
        *{
            margin: 0;
            box-sizing: border-box;
            font-family: 'Inter';
        }

        main{
            width: calc( 100% - 180px );
            min-height: calc( 100vh - 160px );
            margin: auto;
            display: flex;
            align-items: center;
            flex-direction: column;
            padding: 0 20px;
        }

        h1{
            display: block;
            width: 100%;
            margin-top: 40px;
            margin-bottom: 30px;
            color: var(--color-sea);
            text-align: left;
        }

        .informacion{
            display: block;
            width: 100%;
            margin-top: -20px;
            margin-bottom: 20px;
            color: var(--color-carbon);
            text-align: left;
        }

        .contenedor{
            display: flex;
            flex-wrap: wrap;
            justify-content: center;
            align-content: center;
        }

        button-component{
            display: block;
            align-self: flex-end;
            margin-top: 50px;
            margin-bottom: 30px;
        }

        label{
            display: block;
            width: 350px;
            height: 336px;
            background-color: var(--color-cloud);
            color: var(--color-carbon);
            box-shadow: 0px 8px 16px -2px rgba(0, 0, 0, 0.2);
            display: flex;
            flex-direction: column;
            justify-content: space-evenly;
            align-items: center;
            border-radius: 8px;
            margin: 10px;
            position: relative;
            cursor: pointer;
        }

        .active{
            background-color: var(--color-active);
        }

        h2{
            display: block;
            width: 100%;
            font-family: 'Inter';
            font-style: normal;
            font-weight: 700;
            font-size: 24px;
            text-align: center;
            margin: 0 20px;
            color: var(--color-carbon);
        }

        input{
            position: absolute;
            z-index: -10;
        }

        p{
            font-family: 'Inter';
            font-style: normal;
            font-weight: 400;
            font-size: 17px;
            margin: 0 20px;
            text-align: center;
        }

        @media (min-width: 300px) and (max-width: 960px){
            main{
                width: 100%;
            }

            h1{
                margin-top: 50px;
                margin-bottom: 40px;
                text-align: center;
                font-size: 24px;
                font-family: 'Inter';
                font-weight: 700;
            }

            .informacion{
                font-family: 'Inter';
                font-weight: 400;
                font-size: 16px;
                text-align: center;
                margin-bottom: 30px;
            }

            label{
                width: 45%;
                height: 156px;
                margin: 0px 5px;
            }

            button-component{
                display: block;
                align-self: center;
                margin-top: 10px;
                margin-bottom: 10px;
            }

            .contenedor{
                width: 100%;
                justify-content: space-evenly;
            }

            h2{
            font-size: 16px;
            text-align: center;
            margin: 0 10px;
            }

            p{
                font-size: 12px;
                margin: 0 10px;
            }

            img{
                width: 40px;
            }
        }
    `;

    get properties() {
        return {
            debito: { type: String },
            credito: { type: String },
        };
    }

    constructor(){
        super();
        this.credito = '';
        this.debito = '';
    }

    firstUpdated(){
        this.selectTypeTarget();
    }

    selectTypeTarget(){
        const credito = this.shadowRoot.querySelectorAll('input');

        const labelDebito = this.shadowRoot.getElementById('debito');
        const labelCredito = this.shadowRoot.getElementById('credito');

        credito.forEach( radio => {
            radio.addEventListener('change', function() {
                if(credito[0].checked){
                    labelDebito.classList = 'active';
                    labelCredito.classList = '';
                } else{
                    labelDebito.classList = '';
                    labelCredito.classList = 'active';
                }
            })
        })
    }

    getValueSelect(){
        const input = this.shadowRoot.querySelectorAll('input');

        if(input[0].checked){
            console.log('--->', input[0].value)
            console.log('--->', input[0])
            this.showAlert('success', 'Se eligió débito');
        } else{
            console.log('-->', input[1].value)
            console.log('-->', input[1])
            this.showAlert('success', 'Se eligió crédito');
        }
    }

    /**
     * Función que nos permite mostrar alertas
     * @param {*} icon Puede recibir success o error
     * @param {*} title Mensaje que se quiere mostrar
     */
    showAlert(icon, title) {

        const Toast = Swal.mixin({
            toast: true,
            position: "top-end",
            showConfirmButton: false,
            timer: 1000,
            timerProgressBar: true,
            didOpen: (toast) => {
                toast.onmouseleave = Swal.resumeTimer;
            }
        });

        Toast.fire({
            icon,
            title
        });
    }

    render() {
        return html`
            <header-component></header-component>

            <main>
                <h1>Selecciona tu tarjeta</h1>

                <p class="informacion">Solicita tu tarjeta siguiendo estos pasos y comienza a usarla cuando quieras.</p>

                <div class="contenedor">

                    <label id="debito" class="active">
                        <input type="radio" name="tipo" value="debito" checked>
                        <h2>Tarjeta de débito</h2>
                        <img src="${debito}" alt="icono flecha">
                        <p>Las compras que realices se pagaran con tus fondos actuales.</p>
                    </label>

                    <label id="credito">
                        <input type="radio" name="tipo" value="credito">
                        <h2>Tarjeta de crédito</h2>
                        <img src="${credito}" alt="icono de cuadros">
                        <p>Las compras que realices serán financiadas por el banco</p>
                    </label>
                </div>

                <button-component url="/elegir" info="Continuar" @click="${this.getValueSelect}"></button-component>
            </main>

            <footer-component></footer-component>
        `;
    }
}

customElements.define('seleccionar-component', SeleccionarComponent);