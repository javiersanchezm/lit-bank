import { LitElement, html, css } from "lit";
import Swal from 'sweetalert2';

export class ElegirComponent extends LitElement{

    static styles = css`

        *{
            margin: 0;
            box-sizing: border-box;
            font-family: 'Inter';
        }

        main{
            width: calc( 100% - 180px );
            min-height: calc( 100vh - 160px );
            margin: auto;
            display: flex;
            align-items: center;
            flex-direction: column;
            padding: 0 20px;
        }

        h1{
            display: block;
            width: 100%;
            margin-top: 40px;
            margin-bottom: 30px;
            color: var(--color-sea);
            text-align: left;
        }

        .informacion{
            display: block;
            width: 100%;
            margin-top: -20px;
            margin-bottom: 20px;
            color: var(--color-carbon);
            text-align: left;
        }

        .contenedor{
            min-height: 280px;
            display: flex;
            flex-wrap: wrap;
            justify-content: center;
            align-content: center;
        }

        button-component{
            display: block;
            align-self: flex-end;
            margin-top: 50px;
            margin-bottom: 30px;
        }

        section{
            width: 350px;
            height: 232px;
            background-color: var(--color-cloud);
            box-shadow: 0px 8px 16px -2px rgba(33, 62, 127, 0.2);
            display: flex;
            flex-direction: column;
            justify-content: space-evenly;
            align-items: center;
            position: relative;
            top: 30px;
            margin: 30px 10px;
        }

        label{
            width: 100%;
            height: 100%;
            cursor: pointer;
            position: relative;
            color: var(--color-carbon);
        }

        input{
            position: absolute;
            top: 20px;
            left: 20px;
            z-index: -1000;
        }

        .active{
            background-color: var(--color-active);
        }

        h2{
            width: 320px;
            height: 150px;
            background-color: var(--color-sea);
            color: var(--color-cloud);
            text-align: center;
            line-height: 150px;
            border-radius: 8px;
            position: absolute;
            top: -40px;
            font-family: 'Inter';
            font-size: 24px;
            font-weight: 700;
            font-style: normal;
        }

        .contenedor p{
            position: absolute;
            top: 130px;
            padding: 10px 20px;
            font-family: 'Inter';
            font-style: normal;
            font-weight: 400;
            font-size: 16px;
        }

        .target--premium{
            background: blue;
            background: var(--color-target-blue);
            position: absolute;
            right: 15px;
        }

        .target--gold{
            background: gold;
            background: var(--color-target-gold);
            position: absolute;
            right: 15px;
        }

        .target--black{
            background: black;
            background: var(--color-target-black);
            position: absolute;
            right: 15px;
        }

        @media (min-width: 320px) and (max-width: 960px){

            main{
                width: 100%;
                min-height: calc( 100vh - 160px );
            }

            h1{
                margin-top: 30px;
                margin-bottom: 30px;
                text-align: center;
                font-size: 24px;
            }

            .informacion{
                display: block;
                width: 100%;
                padding: 10px;
                font-family: 'Inter';
                font-size: 16px;
                font-weight: 400;
                color: var(--color-carbon);
                text-align: center;
            }

            .contenedor{
                width: 100%;
                height: 100%;
                position: relative;
                color: black;
            }

            section{
                width: 320px;
                height: 90px;
                position: relative;
                top: 0px;
                margin: 15px 10px;
                border-radius: 8px;
                box-shadow: 0px 8px 16px -2px rgba(33, 62, 127, 0.2);
            }

            label{
                border-radius: 10px;
            }

            h2{
                width: 120px;
                height: 50px;
                line-height: 50px;
                position: absolute;
                font-size: 16px;
                top: 20px;
                left: 5px;
            }

            .target p{
                width: 180px;
                position: absolute;
                top: -5px;
                right: -5px;
                font-size: 12px;
            }

            button-component{
                display: block;
                align-self: center;
                margin-top: 10px;
                margin-bottom: 10px;
            }

        }
    `;

    get properties() {
        return {
            premium: { type: String },
            gold: { type: String },
            black: { type: String },
        };
    }

    constructor(){
        super();
        this.premium = '';
        this.gold = '';
        this.black = '';
    }

    firstUpdated(){
        this.selectTypeTarget();
    }

    selectTypeTarget(){
        const input = this.shadowRoot.querySelectorAll('input');

        const labelPremium = this.shadowRoot.getElementById('premium');
        const labelGold = this.shadowRoot.getElementById('gold');
        const labelBlack = this.shadowRoot.getElementById('black');

        input.forEach( radio => {
            radio.addEventListener('change', function() {
                if(input[0].checked){
                    labelPremium.classList = 'active';
                    labelGold.classList = '';
                    labelBlack.classList = '';
                } else if(input[1].checked){
                    labelPremium.classList = '';
                    labelGold.classList = 'active';
                    labelBlack.classList = '';
                } else{
                    labelPremium.classList = '';
                    labelGold.classList = '';
                    labelBlack.classList = 'active';
                }
            })
        })
    }

    getValueSelect(){
        const input = this.shadowRoot.querySelectorAll('input');

        if(input[0].checked){
            console.log('--->', input[0].value)
            console.log('--->', input[0])
            this.showAlert('success', 'Se eligió Premium');
        } else if(input[1].checked){
            console.log('--->', input[1].value)
            console.log('--->', input[1])
            this.showAlert('success', 'Se eligió Gold');
        } else{
            console.log('-->', input[2].value)
            console.log('-->', input[3])
            this.showAlert('success', 'Se eligió Black');
        }
    }

    /**
     * Función que nos permite mostrar alertas
     * @param {*} icon Puede recibir success o error
     * @param {*} title Mensaje que se quiere mostrar
     */
    showAlert(icon, title) {

        const Toast = Swal.mixin({
            toast: true,
            position: "top-end",
            showConfirmButton: false,
            timer: 1000,
            timerProgressBar: true,
            didOpen: (toast) => {
                toast.onmouseleave = Swal.resumeTimer;
            }
        });

        Toast.fire({
            icon,
            title
        });
    }

    render(){
        return html`
            <header-component></header-component>

            <main>
                <h1>Elige el tipo de tarjeta</h1>

                <p class="informacion">Solicita tu tarjeta siguiendo estos pasos y comienza a usarla cuando quieras.</p>

                <div class="contenedor">
                    <!-- <section class="target">
                        <h2 class="target--premium">Bank Premium</h2>
                        <p>Las compras que realices te bonificarán el 2% en puntos, que podrás canjear en tus compras</p>
                    </section> -->

                    <section class="target">
                        <label id="premium" class="active">
                            <input type="radio" name="tipo" value="premium" checked>
                            <h2 class="target--premium">Bank Premium</h2>
                            <p>Las compras que realices te bonificarán el 2% en puntos, que podrás canjear en tus compras</p>
                        </label>
                    </section>

                    <section class="target">
                        <label id="gold">
                            <input type="radio" name="tipo" value="gold">
                            <h2 class="target--gold">Bank Gold</h2>
                            <p>Las compras que realices te bonificarán el 5% en puntos, que podrás canjear en tus compras</p>
                        </label>
                    </section>

                    <section class="target">
                        <label id="black">
                            <input type="radio" name="tipo" value="black">
                            <h2 class="target--black">Bank Black</h2>
                            <p>Las compras que realices te bonificarán el 10% en puntos, que podrás canjear en tus compras</p>
                        </label>
                    </section>

                    <!-- <section class="target">
                        <h2 class="target--gold">Bank Gold</h2>
                        <p>Las compras que realices te bonificarán el 5% en puntos, que podrás canjear en tus compras</p>
                    </section> -->

                    <!-- <section class="target">
                        <h2 class="target--black">Bank Black</h2>
                        <p>Las compras que realices te bonificarán el 10% en puntos, que podrás canjear en tus compras</p>
                    </section> -->
                </div>

                <button-component url="/resumen-solicitud" info="Continuar" @click="${this.getValueSelect}"></button-component>
            </main>

            <footer-component></footer-component>
        `;
    }
}

customElements.define('elegir-component', ElegirComponent);