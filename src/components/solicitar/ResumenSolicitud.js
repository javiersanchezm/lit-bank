import { LitElement, html, css } from 'lit-element';
import Swal from 'sweetalert2';

export class ResumenSolicitud  extends LitElement {

    static styles = css`
        *{
            margin: 0;
            box-sizing: border-box;
            font-family: 'Inter';
        }

        main{
            width: calc( 100% - 180px );
            min-height: calc( 100vh - 160px );
            margin: auto;
            display: flex;
            align-items: center;
            flex-direction: column;
            padding: 0 20px;
        }

        h2{
            display: block;
            width: 100%;
            margin-top: 40px;
            margin-bottom: 30px;
            color: var(--color-sea);
            text-align: left;
        }

        .informacion{
            display: block;
            width: 100%;
            margin-top: -20px;
            margin-bottom: 20px;
            color: var(--color-carbon);
            text-align: left;
        }

        .contenedor{
            display: flex;
            flex-direction: column;
            flex-wrap: wrap;
            justify-content: center;
            align-content: center;
        }

        button-component{
            display: block;
            align-self: flex-end;
            margin-top: 50px;
            margin-bottom: 30px;
        }

        .target-header{
            width: 830px;
            height: 97px;
            background-color: var(--color-cloud);
            box-shadow: 0px 8px 16px -2px rgba(0, 0, 0, 0.2);
            display: flex;
            justify-content: space-between;
            align-items: center;
            border-radius: 8px;
            margin: 10px;
            position: relative;
        }

        .target-header h3{
            color: var(--color-carbon);
            font-family: 'Inter';
            font-weight: 400;
            font-size: 20px;
        }

        .target-header h3, .target-header a,
        .target-body div, .target-body a{
            margin: auto 40px;
        }

        a{
            text-decoration: none;
            color: var(--color-sea);
            font-family: 'Inter';
            font-weight: 700;
            font-size: 20px;
        }

        .target-body div{
            border: 1px solid gray;
            padding: 20px 30px;
            border-radius: 8px;
        }

        .target-body div{
            width: 100%;
            display: flex;
            flex-direction: row;
            justify-content: space-between;
        }

        .target-body div h3{
            width: 320px;
            height: 100px;
            background: blue;
            background: var(--color-target-black);
            color: var(--color-cloud);
            text-align: center;
            line-height: 100px;
            border-radius: 8px;
            font-family: 'Inter';
            font-weight: 700;
            font-size: 16px;
        }

        .target-body div p{
            width: 190px;
            height: 100px;
            font-family: 'Inter';
            font-weight: 400;
            font-size: 14px;
            color: black;
            padding-top: 15px;
            color: var(--color-carbon);
        }

        .target-body{
            width: 830px;
            height: 200px;
            background-color: var(--color-cloud);
            box-shadow: 0px 8px 16px -2px rgba(0, 0, 0, 0.2);
            display: flex;
            justify-content: space-between;
            align-items: center;
            border-radius: 8px;
            margin: 10px;
            position: relative;
        }

        @media (min-width: 300px) and (max-width: 960px){
            main{
                width: 100%;
                height: calc(100vh - 160px);
            }

            h2{
                margin-top: 50px;
                text-align: center;
                font-size: 24px;
            }

            .informacion{
                font-weight: 400;
                font-size: 16px;
                text-align: center;
                margin-bottom: 40px;
            }

            button-component{
                display: block;
                align-self: center;
                margin-top: 10px;
                margin-bottom: 10px;
            }

            .contenedor{
                width: 100%;
                justify-content: space-evenly;
                box-shadow: 0px 8px 16px -2px rgba(0, 0, 0, 0.2);
            }

            .target-body a{
                display: none;
            }

            .target-body, .target-header{
                width: 100%;
                box-shadow: none;
            }

            .target-header{
                width: 100%;
                height: 50px;
            }

            .target-header h3, .target-header a{
                font-size: 16px;
            }

            .target-body div{
                padding: 20px 15px;
                width: 90%;
                margin: auto;
            }

            .target-body div h3{
                width: 150px;
                height: 70px;
                line-height: 70px;
                margin-right: 10px;
            }

            .target-body div p{
                width: 150px;
                height: 70px;
                font-size: 12px;
                padding-top: 0px;
            }

            .target-body{
                width: 100%;
                height: 140px;
                margin-top: -10px;
            }
        }

    `;

    constructor() {
        super();
    }

    getTarget(){
        this.showAlert('success', 'Se creó correctamente');
    }

    /**
     * Función que nos permite mostrar alertas
     * @param {*} icon Puede recibir success o error
     * @param {*} title Mensaje que se quiere mostrar
     */
    showAlert(icon, title) {
        const Toast = Swal.mixin({
            toast: true,
            position: "top-end",
            showConfirmButton: false,
            timer: 1000,
            timerProgressBar: true,
            didOpen: (toast) => {
                toast.onmouseleave = Swal.resumeTimer;
            }
        });

        Toast.fire({
            icon,
            title
        });
    }

    render() {
        return html`
            <header-component></header-component>

            <main>
                <h2>Resumen de la solicitud</h2>

                <p class="informacion">Verifica la información que seleccionaste y confirma tu solicitud.</p>

                <div class="contenedor">
                    <section class="target-header">
                        <h3>Tarjeta de crédito</h3>
                        <a href="/seleccionar">Cambiar</a>
                    </section>
                    <section class="target-body">
                        <div>
                            <h3>Bank Black</h3>
                            <p>Las compras que realices te bonificarán el 5% en puntos, que podrás canjear en tus compras</p>
                        </div>
                        <a href="/elegir">Cambiar</a>
                    </section>
                </div>

                <button-component url="/exito" info="¡Listo solicitar tarjeta!" @click="${this.getTarget}"></button-component>
            </main>

            <footer-component></footer-component>
        `;
    }
}

customElements.define('resumen-solicitud', ResumenSolicitud);
