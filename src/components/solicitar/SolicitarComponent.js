import { LitElement, html, css } from "lit";
import flecha from '/images/Flecha.svg';
import cuadros from '/images/Cuadros.svg';
import correcto from '/images/Correcto.svg';

export class SolicitarComponent extends LitElement{

    static styles = css`

        *{
            margin: 0;
            box-sizing: border-box;
            font-family: 'Inter';
        }

        main{
            width: calc( 100% - 180px );
            min-height: calc(100vh - 160px);
            margin: auto;
            display: flex;
            align-items: center;
            flex-direction: column;
            padding: 0 20px;
        }

        h1{
            display: block;
            width: 100%;
            margin-top: 40px;
            margin-bottom: 30px;
            color: var(--color-sea);
            text-align: left;
        }

        .contenedor{
            display: flex;
            flex-wrap: wrap;
            justify-content: center;
            align-content: center;
        }

        button-component{
            display: block;
            align-self: flex-end;
            margin-top: 50px;
            margin-bottom: 30px;
        }

        img{
            /* width: 110px;
            align-self: center; */
            width: 80px;
        }

        h2{
            display: block;
            width: 100%;
            font-family: 'Inter';
            font-style: normal;
            font-weight: 700;
            font-size: 18px;
            text-align: left;
            margin: 0 20px;
        }

        p{
            font-family: 'Inter';
            font-style: normal;
            font-weight: 400;
            font-size: 17px;
            margin: 0 20px;
        }

        section{
            width: 350px;
            height: 336px;
            background-color: var(--color-cloud);
            box-shadow: 0px 8px 16px -2px rgba(0, 0, 0, 0.2);
            display: flex;
            flex-direction: column;
            justify-content: space-evenly;
            align-items: center;
            border-radius: 8px;
            margin: 10px;
            color: var(--color-carbon);
        }

        .informacion{
            display: none;
        }

        @media (min-width: 300px) and (max-width: 960px){
            main{
                width: 100%;
                min-height: calc(100vh - 160px);
            }

            h1{
                margin-top: 30px;
                margin-bottom: 10px;
                text-align: center;
                font-size: 24px;
            }

            img{
                display: none;
            }

            /* section{
                padding: 50px 10px;
            } */

            section{
                width: 100%;
                max-height: 120px;
                padding: 10px auto;
                margin: 10px 10px;
                align-items: flex-start;
            }

            button-component{
                display: block;
                align-self: center;
                margin-top: 10px;
                margin-bottom: 10px;
            }

            .informacion{
                display: block;
                width: 100%;
                padding: 10px;
                font-family: 'Inter';
                font-size: 16px;
                font-weight: 400;
                color: var(--color-carbon);
            }
        }


    `;

    constructor(){
        super();
    }

    render(){
        return html`
            <header-component></header-component>
    
            <main>
                <h1>Solicitar Tarjeta</h1>

                <p class="informacion">Solicita tu tarjeta siguiendo estos pasos y comienza a usarla cuando quieras.</p>

                <div class="contenedor">
                    <section>
                        <img src="${flecha}" alt="icono flecha">
                        <div>
                            <h2>1. Selecciona la tarjeta que quieres.</h2>
                            <p>Podrás elegir entre una tarjeta de débito o de crédito.</p>
                        </div>
                    </section>

                    <section>
                        <img src="${cuadros}" alt="icono de cuadros">
                        <div>
                            <h2>2. Elige el tipo de tarjeta.</h2>
                            <p>Escoge entre las opciones, la que cumpla con tus necesidades.</p>
                        </div>
                    </section>

                    <section>
                        <img src="${correcto}" alt="icono de correcto">
                        <div>
                            <h2>3. Confirma tú solicitud.</h2>
                            <p>Confirma tu selección y ¡listo! visualiza tus tarjetas en el apartado "Mis tarjetas".</p>
                        </div>
                    </section>
                </div>

                <button-component url="/seleccionar" info="Comenzar solicitud"></button-component>
            </main>

            <footer-component></footer-component>
        `;
    }
}

customElements.define('solicitar-component', SolicitarComponent);