import { LitElement, html, css } from "lit";
import Swal from 'sweetalert2';

export class LoginComponent extends LitElement{

    static styles = css`
        *{
            box-sizing: border-box;
            margin: 0;
        }

        .form-control.success input{
            border: 1px solid var(--color-success);
        }

        .form-control.error input{
            border: 1px solid var(--color-fail);
        }

        span{
            color: var(--color-fail);;
            font-size: 14px;
            display: flex;
            margin: 1px 0px 10px;
            display: none;
        }

        .form-control.error span{
            display: flex;
        }

        .contenedor{
            min-height: calc( 100vh - 70px );
            display: flex;
            justify-content: center;
            background-color: var(--color-primary-brand);
        }

        form{
            display: flex;
            flex-direction: column;
            align-items: center;
            height: 440px;
            width: 500px;
            background-color: var(--color-cloud);
            border-radius: 10px;
            margin: 30px auto;
        }

        h2, p, a{
            font-family: 'Inter';
        }

        h2{
            color: var(--color-sea);
            margin: 20px auto 10px;
        }

        p{
            color: var(--color-carbon);
            margin-bottom: 50px;
            font-weight: 300;
            font-size: 16px;
        }

        .account, a{
            font-size: 14px;
            margin-bottom: 20px;
        }

        a{
            text-decoration: none;
            color: var(--color-sea);
            margin: -15px auto 15px;
        }

        button-component{
            margin-bottom: 10px;
        }

        input{
            display: block;
            margin: 0;
            border: 1px solid gray;
            outline: 0;
            padding-left: 15px;
            width: 390px;
            height: 40px;
            border-radius: 4px;
            color: var(--color-carbon);
            background-color: var(--color-cloud-secundary);
            margin-bottom: 12px;
            font-family: 'Inter';
            font-weight: 400;
            font-size: 14px;
        }

        input:focus{
            color: var(--color-sea);
        }

        @media (min-width: 300px) and (max-width: 960px){
            form{
                width: 100%;
                background-color: var(--color-cloud);
                margin: 0 auto;
                border-radius: 0;
            }

            div{
                background-color: var(--color-cloud);
            }

            input{
                max-width: 300px;
            }

            .contenedor{
                background-color: var(--color-cloud);
            }
        }
    `;

    static properties = {
        email: { type: String },
        password: { type: String},
        url: { type: String }
    }

    constructor(){
        super();
        this.email = '';
        this.password = '';
        this.url = '';
    }

    /**
     * Método que permite iniciar sesión y hace diferentes validaciones de correo y contraseña
     */
    async iniciarSesion(){
        // Loader 2seg simulación

        let correo = this.shadowRoot.getElementById('correo');
        let passw = this.shadowRoot.getElementById('passw');
        let emailValue = correo.value;
        let passwordValue = passw.value;

        if(emailValue !== '' && passwordValue !== ''){
            if( emailValue === "javier200796.20@gmail.com" && passwordValue === "123456"){

                // Método para consumir API, y obtener el token getData()
                // Librería para "Inicio sesión correctamente"

                this.url = '/tarjetas-principal';
                this.showAlert('success', 'Inicio sesión correctamente');

            } else{
                // Librería para error ( O realizar un alert personalizado nativo)
                this.showAlert('error', 'Credenciales inválidas');

                let emailReset = correo.parentElement;
                let passwReset = passw.parentElement;
                let small0 = emailReset.getElementsByTagName('span');
                let small1 = passwReset.getElementsByTagName('span');

                emailReset.className = 'form-control';
                passwReset.className = 'form-control';
                small0[0].innerText = '';
                small1[0].innerText = '';
                correo.value = '';
                passw.value = '';
            }
        }

        else{
            if(emailValue === '') this.setErrorFor('correo', 'Introduzca un correo');
            else if( !this.isEmail(emailValue) ){
                this.setErrorFor('correo', 'No ingreso un correo válido');
                correo.value = '';
                passw.value = '';
            } else this.setSuccessFor('correo');


            if(passwordValue === '') this.setErrorFor('passw', 'Introduzca la contraseña');
            else this.setSuccessFor('passw');
        }
    }

    /**
     * Método para setear de forma correcta el input
     * @param {string} input
     */
    setSuccessFor(input){
        let formControl = this.shadowRoot.getElementById(input);
        formControl = formControl.parentElement;
        formControl.className = 'form-control success';

        let small = formControl.getElementsByTagName('span');
        small[0].innerText = '';
    }

    /**
     * Método para setear el error en el input
     * @param {string} input
     * @param {string} message
     */
    setErrorFor(input, message){
        let formControl = this.shadowRoot.getElementById(input);
        formControl = formControl.parentElement;
        formControl.className = 'form-control error';

        let small = formControl.getElementsByTagName('span');
        small[0].innerText = message;
    }

    /**
     * Método para válidar la estructura de un correo eléctronico
     * @param {string} correo
     * @returns True o False
     */
    isEmail(correo) {
        return /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(correo);
    }

    /**
     * Función que nos permite mostrar alertas
     * @param {*} icon Puede recibir success o error
     * @param {*} title Mensaje que se quiere mostrar
     */
    showAlert(icon, title) {

        const Toast = Swal.mixin({
            toast: true,
            position: "top-end",
            showConfirmButton: false,
            timer: 1000,
            timerProgressBar: true,
            didOpen: (toast) => {
                toast.onmouseleave = Swal.resumeTimer;
            }
        });

        Toast.fire({
            icon,
            title
        });
    }

    render() {
        return html`

            <header-intro></header-intro>

            <div class="contenedor">
                <form id="form">
                    <h2>¡Bienvenid@!</h2>
                    <p>Inicia sesión</p>

                    <div class="form-control">
                        <input type="email" id="correo" placeholder="Correo electrónico">
                        <span></span>
                    </div>

                    <div class="form-control">
                        <input type="password" id="passw" placeholder="Contraseña">
                        <span></span>
                    </div>

                    <button-component info="Iniciar sesión" @click="${this.iniciarSesion}" url="${this.url}"></button-component>

                    <p class="account">¿Aún no tienen una cuenta?</p>
                    <a href="/crear-cuenta">Crea tu cuenta</a>
                </form>
            </div>
        `;
    }
}

customElements.define('login-component', LoginComponent);