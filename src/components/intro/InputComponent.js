import { LitElement, html, css } from "lit";

export class InputComponent extends LitElement{

    static get properties() {
        return {
            type: { type: String },
            name: { type: String },
            id: { type: String }
        }
    }

    static styles = css`
        *{
            margin: 0;
            box-sizing: border-box;
        }

        input{
            display: block;
            margin: 0;
            border: 1px solid gray;
            outline: 0;
            padding-left: 15px;
            width: 390px;
            height: 40px;
            border-radius: 4px;
            color: var(--color-carbon);
            background-color: var(--color-cloud-secundary);
            margin-bottom: 12px;
            font-family: 'Inter';
            font-weight: 400;
            font-size: 14px;
        }

        input:focus{
            color: var(--color-sea);
        }

        @media (min-width: 300px) and (max-width: 960px){
            input{
                max-width: 300px;
            }
        }
    `;

    render(){
        return html`
            <input type="${this.type}" placeholder="${this.name}" id="${this.id}" required>
        `;
    }
}

customElements.define('input-component', InputComponent);