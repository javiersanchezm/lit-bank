import { LitElement, html, css } from "lit";
import vector from '/images/Vector(2).png';


export class NoInternetConectionComponent extends LitElement{

    static styles = css`
        *{
            margin: 0;
            box-sizing: border-box;
        }

        :host{
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
            width: 100%;
            height: 100vh;
        }

        img{
            width: 176px;
            height: 176px;
            margin-bottom: 50px;
        }

        div{
            text-align: center;
        }

        .title{
            color: var(--color-sea);
            font-family: 'Inter';
            font-style: normal;
            font-weight: 700;
            font-size: 24px;
        }

        .paragraph{
            margin: 10px auto 40px;
            color: var(--color-carbon);
            font-family: 'Inter';
            font-weight: 400;
            font-size: 16px;
        }
    `;

    render(){
        return html`
            <img src="${vector}">
            <div>
                <p class="title">Oops!</p>
                <p class="paragraph">No internet connection found. <br>Check your conection or try again</p>
                <button-component info="Try Again" url="https://www.google.com"></button-component>
            </div>
        `;
    }
}

customElements.define('no-internet-conection-component', NoInternetConectionComponent);