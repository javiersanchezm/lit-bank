import { LitElement, html, css } from "lit";

export class IntroComponent extends LitElement{

    static styles = css`
        *{
            margin: 0;
            padding: 0;
            border: none;
            background-color: var(--color-primary-brand);
            box-sizing: border-box;
        }

        main{
            width: 100%;
            min-height: calc( 100vh - 160px );
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
        }

        p{
            margin-bottom: 20px;
            color: var(--color-cloud);
            font-family: 'Inter';
            font-weight: 300;
            font-size: 40px;
        }

        a{
            display: inline-block;
            width: 185px;
            height: 40px;
            margin: auto 10px;
            text-align: center;
            line-height: 40px;
            font-size: 14px;
            text-decoration: none;
            border-radius: 4px;
            box-shadow: var(--box-shadow-button);
        }

        a:nth-child(1){
            background-color: var(--color-sea);
            border: 1px solid var(--color-cloud);
            color: var(--color-cloud);
        }

        a:nth-child(2){
            background-color: var(--color-cloud);
            border: 1px solid var(--color-sea);
            color: var(--color-sea);
        }

        @media (min-width: 300px) and (max-width:960px){

            p{
                margin-left: 20px;
                margin-right: 20px;
                margin-bottom: 30px;
            }

            a{
                width: 140px;
                margin-right: 5px;
                margin-left: 5px;
            }
        }

    `;

    render() {
        return html`
            <header-inicio></header-inicio>

            <main>
                <p>Solicita tus tarjetas en minutos y administralas<br>en <b>un solo lugar.</b></p>
                <div>
                    <a href="/login">Iniciar sesión</a>
                    <a href="/crear-cuenta">Crear cuenta</a>
                </div>
            </main>

            <footer-intro></footer-intro>
        `;
    }
}

customElements.define('intro-component', IntroComponent);