import {LitElement, html, css} from 'lit';

export class FooterIntro extends LitElement{

    static styles =
        css`
            *{
                margin: 0;
                box-sizing: border-box;
            }

            footer{
                width: 100%;
                height: 90px;
                display: flex;
                justify-content: center;
                align-items: center;
                background-color: var(--color-primary-brand);
                color: var(--color-cloud);
                font-family: 'Inter';
                font-weight: 400;
                font-size: 16px;
            }
        `;

    constructor(){
        super();
    }

    render(){
        return html`
            <footer>Ciudad de México, México</footer>
        `;
    }
}

customElements.define('footer-intro', FooterIntro);
