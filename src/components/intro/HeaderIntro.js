import { LitElement, html, css } from "lit";
import angle from '/images/angle-left-solid.svg';
import logo from '/images/logo-bank-red.png';

export class HeaderIntro extends LitElement{

    static styles = css`
        *{
            box-sizing: border-box;
            border: none;
            margin: 0;
        }

        .header{
            width: 100%;
            height: 70px;
            background-color: var(--color-cloud);
            display: flex;
            justify-content: flex-end;
            align-items: center;
            padding: 20px 90px;
        }

        .header__image{
            // width: 214px;
            //height: 39px;
            width: 170px;
            height: 50px;
        }

        .icon{
            width: 35px;
            height: 35px;
            display: none;
        }

        @media (min-width: 300px) and (max-width: 960px){
            .header{
                justify-content: center;
                position: relative;
                justify-content: flex-end;
                padding: 20px 50px;
            }

            .icon{
                position: absolute;
                display: block;
                left: 30px;
                top: 16px;
            }
        }
    `;

    constructor(){
        super();
    }

    render() {
        return html`
            <header class="header">
                <a href="/">
                    <img src="${angle}" class="icon" alt="icono angle left">
                </a>
                <img src="${logo}" class="header__image" alt="logo de bank">
            </header>
        `;
    }
}

customElements.define('header-intro', HeaderIntro);