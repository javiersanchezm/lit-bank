import { LitElement, html, css } from "lit";

export class CreateAccountComponent extends LitElement{

    static styles = css`
        *{
            box-sizing: border-box;
            margin: 0;
        }

        .form-control.success input{
            border: 1px solid var(--color-success);
        }

        .form-control.error input{
            border: 1px solid var(--color-fail);
        }

        span{
            color: var(--color-fail);
            font-size: 14px;
            display: flex;
            margin: 1px 0px 10px;
            display: none;
        }

        .form-control.error span{
            display: flex;
        }

        .contenedor{
            display: flex;
            justify-content: center;
            background-color: var(--color-primary-brand);
            min-height: calc( 100vh - 70px );
        }

        form{
            display: flex;
            flex-direction: column;
            align-items: center;
            width: 100%;
            min-height: 492px;

            width: 500px;
            background-color: var(--color-cloud);
            border-radius: 10px;
            margin: 30px auto;
        }

        h2, p, a{
            font-family: 'Inter';
        }

        h2{
            color: var(--color-sea);
            margin: 20px auto 10px;
        }

        p{
            color: var(--color-carbon);
            margin-bottom: 15px;
            font-weight: 300;
            font-size: 16px;
        }

        .account, a{
            font-size: 14px;
            margin-bottom: 20px;
        }

        a{
            text-decoration: none;
            color: var(--color-sea);
            margin: -15px auto 15px;
        }

        button-component{
            margin-bottom: 10px;
        }

        input{
            display: block;
            margin: 0;
            border: 1px solid gray;
            outline: 0;
            padding-left: 15px;
            width: 390px;
            height: 40px;
            border-radius: 4px;
            color: var(--color-carbon);
            background-color: var(--color-cloud-secundary);
            margin-bottom: 12px;
            font-family: 'Inter';
            font-weight: 400;
            font-size: 14px;
        }

        input:focus{
            color: var(--color-sea);
        }

        @media (min-width: 300px) and (max-width: 960px){
            form{
                width: 100%;
                background-color: var(--color-cloud);
                margin: 0 auto;
                border-radius: 0;
            }

            div{
                background-color: var(--color-cloud);
            }

            input{
                max-width: 300px;
            }

            .contenedor{
                background-color: var(--color-cloud);
            }
        }
    `;

    static properties = {
        nombre : { type: String },
        apellido : { type: String },
        correo : { type: String },
        contraseña1 : { type: String },
        contraseña2 : { type: String },
        url : { type: String }
    }

    constructor(){
        super();
        this.nombre = '';
        this.apellido = '';
        this.correo = '';
        this.contraseña1 = '';
        this.contraseña2 = '';
        this.url = '';
    }

    /**
     * Método para crear cuenta
     */
    createAccount(){
        let nombre = this.shadowRoot.getElementById('nombre');
        let apellido = this.shadowRoot.getElementById('apellido');
        let correo = this.shadowRoot.getElementById('correo');
        let contraseña1 = this.shadowRoot.getElementById('contraseña1');
        let contraseña2 = this.shadowRoot.getElementById('contraseña2');

        if(nombre.value === '') this.setErrorFor('nombre', 'Introduzca su nombre');
        else this.setSuccessFor('nombre');

        if(apellido.value === '') this.setErrorFor('apellido', 'Introduzca su apellido');
        else this.setSuccessFor('apellido');

        if(correo.value === '') this.setErrorFor('correo', 'Introduzca su correo');
        else if( !this.isEmail(correo.value) ){
            this.setErrorFor('correo', 'No ingreso un correo válido');
        } else this.setSuccessFor('correo');

        if(contraseña1.value === '') this.setErrorFor('contraseña1', 'Introduzca su contraseña');
        else this.setSuccessFor('contraseña1');

        if(contraseña2.value === '') this.setErrorFor('contraseña2', 'No puede dejar la contraseña en blanco');
        else if(contraseña2.value !== contraseña1.value) this.setErrorFor('contraseña2', 'Las contraseñas no coinceden');
        else this.setSuccessFor('contraseña2');

        let validacionObject = [
            { val: nombre.parentElement.className},
            { val: apellido.parentElement.className},
            { val: correo.parentElement.className},
            { val: contraseña1.parentElement.className},
            { val: contraseña2.parentElement.className}];

        const validar = validacionObject.every( valor => valor.val == 'form-control success' );

        if( validar ){
            alert('Se registro con éxito');

            let nombreReset = nombre.parentElement;
            let apellidoReset = apellido.parentElement;
            let correoReset = correo.parentElement;
            let contraseña1Reset = contraseña1.parentElement;
            let contraseña2Reset = contraseña2.parentElement;

            nombre.value = '';
            apellido.value = '';
            correo.value = '';
            contraseña1.value = '';
            contraseña2.value = '';
            nombreReset.className = 'form-control';
            apellidoReset.className = 'form-control';
            correoReset.className = 'form-control';
            contraseña1Reset.className = 'form-control';
            contraseña2Reset.className = 'form-control';

            this.url = '/login';
        }
    }

    /**
     * Método para setear de forma correcta el input
     * @param {string} input
     */
    setSuccessFor(input){
        let formControl = this.shadowRoot.getElementById(input);
        formControl = formControl.parentElement;
        formControl.className = 'form-control success';

        let small = formControl.getElementsByTagName('span');
        small[0].innerText = '';
    }

    /**
     * Método para setear el error en el input
     * @param {string} input
     * @param {string} message
     */
    setErrorFor(input, message){
        let formControl = this.shadowRoot.getElementById(input);
        formControl = formControl.parentElement;
        formControl.className = 'form-control error';

        let small = formControl.getElementsByTagName('span');
        small[0].innerText = message;
    }

    /**
     * Método para válidar la estructura de un correo eléctronico
     * @param {string} correo
     * @returns True o False
     */
    isEmail(correo) {
        return /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(correo);
    }

    render() {
        return html`
            <header-intro></header-intro>

            <div class="contenedor">
                <form id="form">
                    <h2>¡Bienvenid@!</h2>
                    <p>Crea tu cuenta</p>

                    <div class="form-control">
                        <input type="text" id="nombre" placeholder="Nombre(s)">
                        <span>Error</span>
                    </div>

                    <div class="form-control">
                        <input type="text" id="apellido" placeholder="Apellido(s)">
                        <span>Error</span>
                    </div>

                    <div class="form-control">
                        <input type="email" id="correo" placeholder="Correo electrónico">
                        <span>Error</span>
                    </div>

                    <div class="form-control">
                        <input type="password" id="contraseña1" placeholder="Contraseña">
                        <span>Error</span>
                    </div>

                    <div class="form-control">
                        <input type="password" id="contraseña2" placeholder="Repetir contraseña">
                        <span>Error</span>
                    </div>

                    <button-component info="Crear cuenta" url="${this.url}" @click="${this.createAccount}"></button-component>

                    <p class="account">¿Ya tienes cuenta?</p>
                    <a href="/login">Inicia sesión</a>
                </form>
            </div>
        `;
    }
}

customElements.define('create-account-component', CreateAccountComponent);