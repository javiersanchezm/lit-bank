import { LitElement, html, css } from "lit";
import angle from '/images/angle-left-solid.svg';
import logo from '/images/logo-bank-red.png';

export class HeaderInicio extends LitElement{

    static styles = css`
        *{
            box-sizing: border-box;
            border: none;
            margin: 0;
        }

        .header{
            width: 100%;
            height: 70px;
            background-color: var(--color-cloud);
            display: flex;
            justify-content: flex-end;
            align-items: center;
            padding: 20px 90px;
        }

        .header__image{
            width: 170px;
            height: 50px;
            /* width: 214px;
            height: 39px; */
        }

        .icon{
            width: 50px;
            height: 50px;
            display: none;
        }

        @media (min-width: 300px) and (max-width: 960px){
            .header{
                justify-content: center;
                position: relative;
                background-color: var(--color-cloud); //var(--color-primary-brand);
                justify-content: flex-end;
                padding: 20px 50px;
            }

            .icon{
                position: absolute;
                display: none;
                left: 30px;
                top: 10px;
            }
        }
    `;

    constructor(){
        super();
    }

    render() {
        return html`
            <header class="header">
                <a href="/">
                    <img src="${angle}" class="icon" alt="icono angle left">
                </a>
                <img src="${logo}" class="header__image" alt="logo de bank">
            </header>
        `;
    }
}

customElements.define('header-inicio', HeaderInicio);