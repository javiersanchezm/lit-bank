import { LitElement, html, css } from "lit";

export class MainPrincipalComponent extends LitElement{

    static styles =
        css`
            :host {
                display: block;
            }

            *{
                box-sizing: border-box;
                border: 0;
                margin: 0;
            }

            .main{
                width: 100%;
                //height: 400px;
                min-height: calc(100vh - 160px);
                display: flex;
                align-content: center;
                /* background-color: var(--color-cloud-secundary); */
            }

            .main section{
                background-color: var(--color-cloud);
                margin: 30px 90px;
                padding: 40px 90px;
                display: flex;
                flex-direction: column;
                justify-content: space-evenly;
                width: 100%;
                font-family: 'Inter';
                text-align: center;
                box-shadow: 0px 8px 16px -2px rgba(0, 0, 0, 0.2);
            }

            .main__title{
                align-self: flex-start;
                font-style: normal;
                font-size: 32px;
                font-weight: 700;
                color: var(--color-sea);
            }

            .main__information{
                display: flex;
                flex-direction: column;
                align-items: center;
                justify-content: center;
            }

            .main__information h3{
                width: 300px;
                height: 58px;
                font-weight: 700;
                font-size: 24px;
                margin-bottom: 10px;
                color: var(--color-carbon);
            }

            .main__information p{
                width: 350px;
                height: 40px;
                padding: 0 30px;
                font-weight: 400;
                font-size: 16px;
                color: var(--color-carbon);
            }

            .button{
                display: block;
                width: 390px;
                height: 38px;
                margin-left: auto;
                margin-right: auto;
                margin-bottom: 20px;
                padding-top: 10px;

                font-family: 'Inter';
                font-style: normal;
                font-weight: 700;
                font-size: 14px;

                background-color: var(--color-sea);
                color: var(--color-cloud);
                border-radius: 4px;
                box-shadow: 0px 16px 32px -4px rgba(33, 62, 127, 0.2);
                cursor: pointer;
                text-decoration: none;
            }

            @media (min-width: 300px) and (max-width:960px)
            {
                .main{
                    padding: 0;
                    margin: 0;
                    min-height: calc(100vh - 160px);
                }


                .main section{
                    margin: 0;
                    box-shadow: none;
                    /* background-color: var(--color-cloud-secundary); */
                    padding: 20px 10px;
                    background-color: transparent;
                }

                .main__title{
                    align-self: center;
                    margin-top: 10px;
                    font-size: 24px;
                }

                .main__information h3{
                    font-size: 24px;
                }

                .main__information p{
                    width: 320px;
                    padding: 0px 20px;
                    font-size: 16px;
                }

                .button{
                    width: 100%;
                    max-width: 320px;
                    font-size: 14px;
                }
            }
        `;

    render(){
        return html`
            <header-component></header-component>
    
            <main class="main">
                <section>
                    <h2 class="main__title">Mis tarjetas</h2>

                    <div class="main__information">
                        <h3>Aún no tienes tarjetas registradas</h3>
                        <p>Solicita tu tarjeta en minutos y comienza a usarla cuando quieras</p>
                    </div>

                    <a class="button" href="/tarjetas">¡Quiero mi tarjeta!</a>
                </section>
            </main>

            <footer-component></footer-component>
        `;
    }
}

customElements.define('main-principal-component', MainPrincipalComponent);