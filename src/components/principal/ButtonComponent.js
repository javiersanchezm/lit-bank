import { LitElement, html, css } from "lit";
import { ifDefined } from 'lit/directives/if-defined.js';

export class ButtonComponent extends LitElement{
    static get properties(){
        return {
            info: { type: String },
            url: { type: String }
        }
    }

    static styles = css`
            *{
                margin: 0;
                box-sizing: border-box;
            }

            a{
                display: block;
                width: 390px;
                height: 38px;
                margin: 20px auto;
                line-height: 35px;
                text-align: center;

                font-family: 'Inter';
                font-style: normal;
                font-weight: 700;
                font-size: 14px;

                background-color: var(--color-sea);
                color: var(--color-cloud);
                border-radius: 4px;
                box-shadow: 0px 16px 32px -4px rgba(33, 62, 127, .6);
                cursor: pointer;
                text-decoration: none;
            }

            @media (min-width: 300px) and (max-width:960px)
            {
                a{
                    /* width: 100%; */
                    max-width: 300px;
                    font-size: 14px;
                }
            }
    `;

    class(){
        inicializeProperties();
    }

    inicializeProperties(){
        this.info = '';
        this.url = '';
    }

    render(){
        return html`
            <a href=${ifDefined(this.url)}> ${this.info} </a>
        `;
    }
}

window.customElements.define('button-component', ButtonComponent);