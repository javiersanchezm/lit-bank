import {LitElement, html, css} from 'lit';
import logo from '/images/logo-bank-red.png';

export class FooterComponent extends LitElement{

    static styles =
        css`
            *{
                margin: 0;
                box-sizing: border-box;
            }

            .footer{
                background-color: var(--color-primary-brand);
                width: 100%;
                height: 90px;
                display: flex;
                flex-direction: column;
                justify-content: center;
                align-items: center;
                text-align: center;
                border-top: 1px solid rgb(191, 191, 247);
            }

            .footer img{
                display: none;
                width: 170px;
                height: 50px;
            }

            .footer__direction{
                font-family: 'Inter';
                font-weight: 400;
                font-size: 16px;
                padding-top: 10px;
                color: var(--color-cloud);
            }


            @media (min-width: 300px) and (max-width:960px)
            {
                .footer{
                    background-color: var(--color-cloud);
                    padding-top: 20px;
                }

                .footer img{
                    margin: auto;
                    display: block;
                }

                .footer__direction{

                    padding-bottom: 20px;
                    color: var(--color-sea);
                    font-size: 1rem;
                }
            }
        `;

    constructor(){
        super();
    }

    render(){
        return html`
            <footer class="footer">
                <img src="${logo}" alt="Logo de Bank">
                <p class="footer__direction">Ciudad de México, México</p>
            </footer>
        `;
    }
}

customElements.define('footer-component', FooterComponent);
