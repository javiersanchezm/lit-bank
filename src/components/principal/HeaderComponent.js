import {LitElement, html, css } from 'lit';
import logo from '/images/logo-bank-red.png';
import xmark from '/images/xmark-solid.svg';
import card from '/images/credit-card-regular.svg';
import comments from '/images/comments-regular.svg';
import arrow from '/images/arrow-right-from-bracket-solid.svg';
import bars from '/images/bars-solid.svg';

export class HeaderComponent extends LitElement {

    static styles = css`
        *{
            box-sizing: border-box;
            border: 0;
            margin: 0;
        }

        .header{
            width: 100%;
            height: 70px;
            background-color: var(--color-cloud);
            font-family: 'Inter';
            font-size: 16px;
            font-weight: 700;
            display: flex;
            justify-content: space-between;
            align-items: center;
            padding: 20px 90px;
        }

        .header__image{
            // width: 214px;
            // height: 39px;
            width: 170px;
            height: 50px;
        }

        .header__nav--link{
            color: var(--color-carbon);
            margin-left: 30px;
            text-decoration: none;
        }

        .header__bar--open{
            width: 30px;
            height: 30px;
            display: none;
            cursor: pointer;
        }

        .header__bar--close{
            width: 30px;
            height: 30px;
            display: none;
            cursor: pointer;
        }

        .header__title, .header__email, .header__title--nav{
            display: none;
        }

        .credit-card, .comments, .sign{
            width: 30px;
            height: 30px;
            margin-right: 15px;
            display: none
        }

        .header__nav--link p{
                display: inline-block;
        }

        @media (min-width: 300px) and (max-width:960px)
        {

            .header{
                position: relative;
                display: flex;
                align-items: center;
                justify-content: center;
                font-family: 'Inter';
                font-style: normal;
                border-bottom: 1px solid rgb(191, 191, 247);
                background-color: var(--color-cloud-secundary);
            }

            .header__bar--open{
                display: block;
                align-self: center;
                position: absolute;
                left: 30px;
            }

            .header__nav{
                position: fixed;
                left: 0;
                top: 0;
                background-color: var(--color-cloud);
                width: 80%;
                height: 100vh;
                z-index: 20;
                color: var(--color-carbon);
                display: flex;
                flex-direction: column;
                align-items: center;
                transform: translateX(150%);
                box-shadow: 200px 0px 0px 20px rgba(0, 0, 0, 0.8);
            }

            .header__bar--close{
                display: block;
                position: absolute;
                top: 15px;
                right: 60px;
                z-index: 20;
                margin-right: -30px;
                color: var(--color-carbon);
            }

            .open{
                transform: translateX(0%);
            }


            .header__image{
                display: none;
            }

            .header__title{
                display: block;
                text-align: center;
                line-height: 2.25rem;
                font-weight: 400;
                font-size: 1rem;
                color: var(--color-sea);
            }

            .header__title--nav{
                display: block;
                margin-top: 60px;
                color: var(--color-sea);
            }

            .header__email{
                display: block;
                width: 100%;
                text-align: center;
                margin-top: 20px;
                padding-bottom: 40px;
                border-bottom: 1px solid rgb(206, 206, 206);

                font-family: 'Inter';
                font-weight: 400;
                font-size: 16px;
            }

            .header__nav--link{
                /* display: block; */
                width: 100%;
                color: var(--color-carbon);
                margin-left: 0px;
                border-bottom: 1px solid rgb(206, 206, 206);
                padding: 10px;
                display: flex;
                justify-items: center;
            }

            .header__nav--link p{
                line-height: 30px;
            }

            .credit-card, .comments, .sign{
                display: inline-block;
                margin: auto 30px;
            }

            .button{
                width: 350px;
                font-size: 0.875rem;
            }
        }
    `;

    static get properties(){
        return{
            claseNav:{type: String},
            claseClose:{type: String}
        }
    }

    constructor()
    {
        super();
        this.claseNav='header__nav';
        this.claseClose='header__bar--close';
    }

    cierraMenu()
    {
        this.claseNav='header__nav';
        this.claseClose='header__bar--close';
    }

    abreMenu()
    {
        this.claseNav='header__nav open';
        this.claseClose='header__bar--close open';
    }

    render() { return html `
        <header class="header">
            <img src="${logo}" class="header__image" alt="logo de bank">

            <nav class="${this.claseNav}">
                <img class="${this.claseClose}" src="${xmark}" alt="icono bar open" @click="${this.cierraMenu}">
                <h2 class="header__title--nav">¡Hola Javier!</h2>
                <p class="header__email">javier200796.20@gmail.com</p>
                <a href="/tarjetas" class="header__nav--link">
                    <img src="${card}" class="credit-card" alt="icono targeta"><p>Mis tarjetas</p></a>
                <a href="/solicitar" class="header__nav--link">
                    <img src="${comments}" class="comments" alt="icono comentarios"><p>Solicitar tarjetas</p></a>
                <a href="/" class="header__nav--link">
                    <img src="${arrow}" class="sign" alt="icono cerrar sesión"><p>Cerrar sesión</p></a>
            </nav>

            <img class="header__bar--open" src="${bars}" alt="icono bar close" @click="${this.abreMenu}">

            <h2 class="header__title">¡Hola Javier!</h2>
        </header>
    `;
    }
}

customElements.define('header-component', HeaderComponent);
