import { LitElement, html, css } from "lit";
import visa from '/images/visa.png';

export class TarjetasComponent extends LitElement{

    static styles =
        css`
            :host {
                display: block;
            }

            *{
                box-sizing: border-box;
                border: 0;
                margin: 0;
                font-family: 'Inter';
            }

            .main{
                width: 100%;
                max-height: 500px;
                min-height: calc(100vh - 160px);
                display: flex;
                padding: 20px 90px;
                background-color: var(--color-cloud)
            }

            .main__title{
                display: flex;
                align-self: flex-start;
                font-style: normal;
                font-size: 32px;
                font-weight: 700;
                color: var(--color-sea);
                width: 300px;
            }

            .main article{
                padding: 0px 40px;
                display: flex;
                flex-direction: column;
                align-items: center;
                justify-content: space-evenly;
                width: 100%;
                height: 450px;
                font-family: 'Inter';
                text-align: center;
                box-shadow: 0px 8px 16px -2px rgba(0, 0, 0, 0.2);
            }

            .main .main__targets{
                display: flex;
                align-items: center;
                flex-wrap: wrap;
                justify-content: center;
                width: 100%;
                min-height: 100px;
            }

            .main .main__target{
                color: var(--color-cloud);
                width: 320px;
                height: 158px;
                border-radius: 16px;
                font-size: 16px;
                margin: 10px 10px;
                padding: 10px 20px;

                display: flex;
                flex-direction: column;
                justify-content: space-evenly;
            }

            .main .main__target div{
                width: 100%;
                display: flex;
                align-items: center;
                justify-content: space-between;
            }

            .main .main__target .target-p1,
            .main .main__target .target-p2,
            .main .main__target .target-p3{
                display: flex;
                align-self: flex-start;
            }

            /* .main .main__target .target-p1 h2,
            .main .main__target .target-p3 h2{
                justify-content: space-around;
            } */

            .main .main__target img{
                align-self: flex-end;
            }

            .main .main__target .target-p1 h2{
                font-family: 'Inter';
                font-size: 16px;
                font-weight: 700;
            }

            .main .main__target .target-p1 p{
                font-family: 'Inter';
                font-size: 14px;
                font-weight: 400;
            }

            .main .main__target .target-p2{
                font-family: 'Inter';
                font-size: 16px;
                font-weight: 400;
            }

            .main .main__target .target-p3 h2{
                font-family: 'Inter';
                font-size: 16px;
                font-weight: 400;
            }

            .main .main__target .target-p3 p{
                font-family: 'Inter';
                font-size: 16px;
                font-weight: 400;
            }

            .main .main__target .target-p3 p small{
                font-family: 'Inter';
                font-size: 10px;
                font-weight: 400;
            }

            .main .main__target--premium{
                background: blue;
                background: var(--color-target-blue);
            }

            .main .main__target--oro{
                background: #997C15;
                background: var(--color-target-gold)
            }

            .main .main__target--black{
                background: #01030D;
                background: var(--color-target-black);
            }

            .main .main__target--add a{
                text-decoration: none;
                color: var(--color-carbon);
                font-size: 50px;
            }

            .main .main__target--add h2{
                font-family: 'Inter';
                font-size: 20px;
                font-weight: 700;
            }

            .main .main__target--add{
                border: 4px solid #7F7F7F;
                color: var(--color-carbon);
                background: #EDEDED;
            }

            button-component{
                display: none;
            }

            @media (min-width: 320px) and (max-width:960px)
            {
                .main{
                    min-height: calc(100vh - 160px);
                }

                .main article{
                    box-shadow: none;
                    height: 450px;
                    display: flex;
                    flex-direction: column;
                    align-items: center;
                    justify-content: start;
                }

                .main__title{
                    align-self: center;
                    margin-top: 20px;
                    margin-bottom: 20px;
                    font-size: 1.5rem;
                }

                .main .main__target{
                    width: 300px;
                    height: 158px;
                }

                .main .main__target--premium .target-p1 h2{
                    font-size: 12px;
                }

                .main .main__target--premium .target-p1 p{
                    font-size: 10px;
                }

                .main .main__target--oro .target-p1 h2{
                    font-size: 14px;
                }

                .main .main__target--oro .target-p1 p{
                    font-size: 12px;
                }

                .main .main__target--black .target-p1 h2{
                    font-size: 16px;
                }

                .main .main__target--black .target-p1 p{
                    font-size: 14px;
                }

                .main .main__target:nth-child(2){
                    width: 285px;
                    height: 158px;
                }

                .main .main__target:nth-child(1){
                    width: 270px;
                    height: 158px;
                }

                .main .main__target:nth-child(2){
                    width: 285px;
                    height: 158px;
                }

                .main .main__targets{
                    position: relative;
                    width: 100%;
                    min-height: 280px;
                }

                .main .main__targets .main__target--premium{
                    position: absolute;
                    top: 0px;
                    z-index: 1;
                }

                .main .main__targets .main__target--oro{
                    position: absolute;
                    top: 50px;
                    z-index: 2;
                }

                .main .main__targets .main__target--black{
                    position: absolute;
                    top: 100px;
                    z-index: 3;
                }

                .main .main__targets .main__target--add{
                    display: none;
                }

                button-component{
                    display: block;
                }
            }
        `;

    render(){
        return html`
                <header-component></header-component>

                <main class="main">
                    <article>
                        <h2 class="main__title">Mis tarjetas</h2>

                        <div class="main__targets">
                            <section class="main__target main__target--premium" id="premium">
                                <div class="target-p1">
                                    <h2>Bank Premium</h2>
                                    <p>Tarjeta de crédito</p>
                                </div>
                                <p class="target-p2">4083 4212 7630 342</p>
                                <div class="target-p3">
                                    <h2>Javier Sánchez</h2>
                                    <p><small>Vencimiento</small> 08/27</p>
                                </div>
                                <img src="${visa}" alt="Logo Visa">
                            </section>

                            <section class="main__target main__target--oro" id="oro">
                                <div class="target-p1">
                                    <h2>Bank Gold</h2>
                                    <p>Tarjeta de crédito</p>
                                </div>
                                <p class="target-p2">4083 4212 7630 342</p>
                                <div class="target-p3">
                                    <h2>Javier Sánchez</h2>
                                    <p><small>Vencimiento</small> 08/27</p>
                                </div>
                                <img src="${visa}" alt="Logo Visa">
                            </section>

                            <section class="main__target main__target--black" id="black">
                                <div class="target-p1">
                                    <h2>Bank Black</h2>
                                    <p>Tarjeta de crédito</p>
                                </div>
                                <p class="target-p2">4083 4212 7630 342</p>
                                <div class="target-p3">
                                    <h2>Javier Sánchez</h2>
                                    <p><small>Vencimiento</small> 08/27</p>
                                </div>
                                <img src="${visa}" alt="Logo Visa">
                            </section>

                            <section class="main__target main__target--add">
                                <a href="/solicitar">+</a>
                                <h2>Solicitar nueva tarjeta</h2>
                            </section>
                        </div>

                        <button-component url="/solicitar" info="Solicitar una tarjeta"></button-component>
                    </article>
                </main>

            <footer-component></footer-component>
        `;
    }
}

customElements.define('tarjetas-component', TarjetasComponent);